# Fehler oder Verbesserungsvorschläge zur Föderalen IT-Infrastruktur melden

Hallo! :wave:

Im Auftrag des [IT-Planungsrat](https://www.it-planungsrat.de/) betreibt die Föderale IT-Kooperation (FITKO) eine Reihe von [Produkten](https://www.fitko.de/produktmanagement) und steuert [Projekte](https://www.fitko.de/projektmanagement), die noch zu Produkten werden wollen.

Um dabei immer besser zu werden, sind wir auf Dein Feedback angewiesen! In diesem Issue-Tracker kannst Du Fehler oder Verbesserungsvorschläge zu Komponenten der Föderalen IT-Infrastruktur melden.

Dieser Issue-Tracker wurde im Rahmen des [Föderalen Entwicklungsportals](https://docs.fitko.de/) im Projekt [FIT-Connect](https://www.fitko.de/projektmanagement/fit-connect) aufgebaut. Der Issue-Tracker soll bestehende Feedback-Möglichkeiten nicht ersetzen, sondern diese ergänzen. Wir versuchen daher gerne, Dein Anliegen an die richtige Ansprechperson zu vermitteln. Bitte habe Verständnis, wenn dies manchmal etwas Zeit in Anspruch nimmt. Wir experimentieren hier gerade mit neuen Feedback-Möglichkeiten und bekommen im Alltagsgeschäft natürlich auch Feedback über weitere Kanäle, das es ebenfalls zu bedienen gilt.

Bitte beachte, dass Dein Feedback öffentlich einsehbar sein wird.

Schau vor Deinem Feedback bitte [in die Liste der bestehenden Issues](https://gitlab.opencode.de/fitko/feedback/-/issues) und erstelle erst dann [ein neues Issue](https://gitlab.opencode.de/fitko/feedback/-/issues/new).

Danke! :blush:

### Weitere Issue-Tracker der öffentlichen Verwaltung
Neben diesem Issue-Tracker zur föderalen IT-Infrastruktur gibt es weitere öffentlich einsehbare Issue-Tracker der Verwaltung. Du kennst weitere Issue-Tracker? [Lass es uns wissen](https://gitlab.opencode.de/fitko/feedback/-/issues/new)!

- [Issue-Tracker zu FIT-Connect & dem Föderalen Entwicklungsportal](https://git.fitko.de/fit-connect/planning/-/issues/): Der verlinkte, öffentlich lesbare Issue-Tracker dient der internen Priorisierung und Umsetzung von Issues. Über neue Issues von Dir zu [FIT-Connect](https://docs.fitko.de/fit-connect/) und dem [Föderalen Entwicklungsportal](https://docs.fitko.de/) freuen wir uns jedoch am meisten hier im Issue-Tracker zur föderalen IT-Infrastruktur.
- [Issue-Tracker zum Standard XZuFI](https://www.pivotaltracker.com/n/projects/2558236): Der Standard [XZuFI (XZuständigkeitsfinder)](https://www.xrepository.de/details/urn:xoev-de:fim:standard:xzufi) standardisiert den Austausch von Zuständigkeiten und weiteren Informationen über Verwaltungsleistungen.
- [Issue-Tracker zum Standard XDomea](https://projekte.kosit.org/groups/xdomea/-/issues): Der Standard [XDomea](https://www.xdomea.de) standardisiert die Übermittlung von Akten, Vorgängen und Dokumenten.
- [Issue-Tracker zum Standard XRechnung](https://projekte.kosit.org/groups/xrechnung/-/issues): Der Standard [XRechnung](https://www.xoev.de/de/xrechnung) standardisiert die elektronische Rechnungsstellung bei öffentlichen Auftraggebern.
